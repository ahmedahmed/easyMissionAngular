import { Account } from './../../Models/account';
import { AccountService } from './../../Services/account/account.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http,Headers } from '@angular/http'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
 userIsConnected:boolean;
headers = new Headers();

  constructor(private as:AccountService,private router:Router,private account:Account,private http:Http) {
    
   }

  ngOnInit() {
   
    this.userIsConnected=this.as.getuserIsConnected();
    if(this.userIsConnected==true)
      {
    this.headers.append('Authorization',localStorage.getItem('token'));
    this.http.get('http://localhost:53472/api/accounts/5',{headers:this.headers}).// creation de l'observable
    subscribe(resp=>{// on difinit l'observer
      this.account=resp.json(); // traitement a faire
    }); 
      }
  }
  logout(){
    localStorage.removeItem('token');
    this.as.setuserIsConnected(false);
    this.router.navigate(['/home']);

  }  
}
