import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../Services/account/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
x:any[];
t:any;
o:any;
  constructor(private ms:AccountService,private router:Router) { }

  ngOnInit() {
    if(this.ms.getuserIsConnected()==false)
    {
      this.router.navigate(['/login']);
    }
    else{
   this.ms.getProfiles("").subscribe(resp=>{
     this.x=resp;
    
     localStorage.removeItem('visite');
     
   });
  }
  }

  find(s){
    this.ms.getProfiles(s).subscribe(resp=>{
      this.x=resp;
    });
  }
  details(id){
  
  localStorage.setItem('visite',id);
  this.router.navigate(['/visite']);
  }
}
