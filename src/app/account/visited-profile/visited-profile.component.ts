import { FullAddress } from './../../Models/address';
import { Http ,Headers} from '@angular/http';
import { Account, FullAccount } from './../../Models/account';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountService } from "../../Services/account/account.service";
import { Address } from "../../Models/address";

@Component({
  selector: 'app-visited-profile',
  templateUrl: './visited-profile.component.html',
  styleUrls: ['./visited-profile.component.css']
})
export class VisitedProfileComponent implements OnInit {
  type="simple";
  headers = new Headers();
  uploadFile: any;
  x:any;
  address:any;
  hasBaseDropZoneOver: boolean = false;
  image;
  options: Object = {
    url: 'http://localhost/upload/upload.php'
  };
  sizeLimit = 2000000;
  constructor(private ms:AccountService,private router:Router,private account:FullAccount,private http:Http) {

   }

  ngOnInit() {
    if(this.ms.getuserIsConnected()==false)
    {
      this.router.navigate(['/login']);
    }
    else{
    this.ms.getCurrentUser().
    subscribe(resp=>{// on difinit l'observer
      this.account=resp.json(); // traitement a faire
      this.address=this.account.address; 
      this.type=this.account.DTYPE;
      this.image=this.account.photo;
      
      
    
    }); 
    }
      
  }
  saveUpdates(){
    
    let account:any;
    if(this.type=="Simple") 
    {
     console.log(this.account.address.gouvernorat); 
          account = {"Simple": {"address": {"id":this.address.Id,"account": null,"municipalite": this.address.Municipalite,"rue": this.address.Rue,"gouvernorat": this.address.Gouvernorat },"cv": null,"firstname": this.account.firstname,"lastname": this.account.lastname,"gender": this.account.gender,"photo": this.image,"id": this.account.Id,"login": this.account.Login,"active": true,"password": this.account.Password,"lastConnection": null,"bannned": false,"email": this.account.Email,"banneEnd": "2017-10-19","phone": this.account.Phone,"about": this.account.About,"dateCreation": "2017-10-10"}
    }
}
   
    
  
   
    this.ms.updateProfile(account).subscribe(resp=>{
   this.router.navigate(['/profile']);
    });
  }


  handleUpload(data): void {
    if (data && data.response) {
      data = JSON.parse(data.response);
      
      this.uploadFile = data;
      this.image=this.uploadFile.generatedName;
    }
  }
 
  fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }
 
  beforeUpload(uploadingFile): void {
    if (uploadingFile.size > this.sizeLimit) {
      uploadingFile.setAbort();
      alert('File is too large');
    }
  }

}
