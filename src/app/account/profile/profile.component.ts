import { CvService } from './../../Services/cv/cv.service';
import { Address } from './../../Models/address';
import { Account } from './../../Models/account';
import { AccountService } from './../../Services/account/account.service';
import { Component, OnInit } from '@angular/core';
import { Http,Headers } from '@angular/http'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  private type:string;
  headers = new Headers();
  private cvList:any[];
  constructor(private as:AccountService,private router:Router,private account:Account,private address:Address,private http:Http,private cv:CvService) {
  
    /*this.as.getCurrentUser().subscribe(resp=>{// on difinit l'observer
      this.account=resp.json(); // traitement a faire
      this.address=this.account.address; 
    });*/
   }

  ngOnInit() {

    if(this.as.getuserIsConnected()==false)
    {
      this.router.navigate(['/login']);
    }    
    else
    {
    this.headers.append('Authorization',localStorage.getItem('token'));
    return this.http.get('http://localhost:53472/api/accounts/5',{headers:this.headers}).
    subscribe(resp=>{// on difinit l'observer
      this.account=resp.json(); // traitement a faire
      this.address=this.account.address; 
      this.type=this.account.DTYPE;
      this.cv.getCVofUser(this.account.Id).subscribe(resp=>this.cvList=resp);

    }); 
   
    }
  }
  
}
