import { SimpleAccount } from './../../Models/sipmleAccount';
import { AccountService } from './../../Services/account/account.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FacebookService, InitParams } from 'ngx-facebook';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  [x: string]: any;
  loginStatus="ok";
  
  constructor(private as:AccountService,private router:Router,private fb: FacebookService) { 
    let initParams: InitParams = {
      appId: '538005939874684',
      xfbml: true,
      version: 'v2.11'
    };
 
    fb.init(initParams);
  }

  ngOnInit() {
  }

  signup(login:string,pwd:string)
  {
   this.as.login(login,pwd).subscribe(resp=>{
   localStorage.setItem('token', resp.text());
   this.as.setuserIsConnected(true);
   this.router.navigate(['/profile']);
    },
   error=>{
    this.loginStatus="error"
   });
  }

  facebookSignup(){
    this.fb.api('/me?fields=id,name,email,about,gender,first_name,last_name,address,birthday,picture,hometown,education,languages','get' ,{access_token : 'EAACEdEose0cBAFuvICTPPLnwvlGJvYxz2cdtBCqQdnZA4ABxgsZAxzu4eP996QVuAFK4QhiNBaZBwsZA7y3210WZC8OrKDWAVrxc5fPetj1tcFXTdnB6lvBJEHSMWXMMb8jsnTqytAcG7ebTak8eeIMGp2z797jkyV77IpoV1mOit8OAN3YS2sg5Jd5PNJhQZD'})
  .then((res: any) => {
    let about="education ";
    let edu:any[];
    edu=res.education;
    for (let i = 0; i < edu.length ; i++) 
    {
       about = about+" "+edu[i].school.name;
    }
    let login=res.first_name;
    let Saccount = new SimpleAccount(about,true,null,false,new Date().toISOString(),"aaaa@gmail.com",null,"t1","t1",24242424,res.first_name,res.gender,res.last_name,res.picture.data.url,{"gouvernorat":res.hometown.name,"rue":"","municipalite":""});
    let account = {"Simple": Saccount}; 
    this.as.register(account).subscribe(resp=>{
      this.as.login("t1","t1").subscribe(resp=>{
        localStorage.setItem('token', resp.text());
        this.as.setuserIsConnected(true);
        this.router.navigate(['/profile']);
    });
    
    });
    
  })
  .catch(this.handleError);  
}  
}
